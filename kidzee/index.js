var images = {
    "triangle"  : "./images/triangle.jpeg",
    "square" : "./images/square.jpeg",
    "circle" : "./images/circle.jpeg",
    "rectangle"   : "./images/rectangle.jpeg",
    "sun"   : "./images/sun.png",
    "cloud"   : "./images/cloud.png",
    "orchid"   : "./images/orchid.png",
    "flowerplant"   : "./images/flowerplant.png",
   
    }  
    function quizResult() {
    if (quiz.isEnded()) {
    showResult();
    } else {
    // show question
    var element = document.getElementById("question");
    element.innerHTML = quiz.getQuestionIndex().text;
    
    
    // show options
    var choices = quiz.getQuestionIndex().choices;
    for (var i = 0; i < choices.length; i++) {
    var element = document.getElementById("choice" + i);
    element.innerHTML = images[choices[i]]? '<img src="'+images[choices[i]]+'">':choices[i];
     
    guess("btn" + i, choices[i]);
    }
    showProgress();
    }
    };
    
    function guess(id, guess) {
    var button = document.getElementById(id);
    button.onclick = function() {
    quiz.guess(guess);
    quizResult();
    }
    };
    
    function showProgress() {
    var currentQuestionNumber = quiz.questionIndex + 1;
    var element = document.getElementById("progress");
    element.innerHTML = "Question " + currentQuestionNumber + " of " + quiz.questions.length;
    };
    
    function showResult() {
    var gameOverHTML = "<h2>Result</h2>";
    gameOverHTML += "<h2 id='result'> Sucessfully completed </h2>";
    var element = document.getElementById("quiz");
    element.innerHTML = gameOverHTML;
    };
    
    //  questions
    var questions = [
    new Question("Click on a circle shaped object", ["circle", "rectangle", "square", "triangle","flowerplant", "sun", "cloud", "orchid"], "circle"),
    new Question("Click on a rectangle shaped object", ["circle", "rectangle", "square", "triangle","flowerplant", "sun", "cloud", "orchid"], "rectangle"),
    new Question("Click on a triangle shaped object", ["circle", "rectangle", "square", "triangle","flowerplant", "sun", "cloud", "orchid"], "triangle"),
    new Question("Click on a square shaped object", ["circle", "rectangle", "square", "triangle","flowerplant", "sun", "cloud", "orchid"], "square"),
    new Question("Click on a cloud", ["circle", "rectangle", "square", "triangle","flowerplant", "sun", "cloud", "orchid"], "cloud"),
    new Question("Click on a Flowerplant", ["circle", "rectangle", "square", "triangle","flowerplant", "sun", "cloud", "orchid"], "flowerplant")

    ];
    
    function Question(text, choices, answer) {
    this.text = text;
    this.choices = choices;
    this.answer = answer;
    }
    
    Question.prototype.isCorrectAnswer = function(choice) {
    return this.answer === choice;
    }
    
    
    
    function Quiz(questions) {
    
    this.questions = questions;
    this.questionIndex = 0;
    }
    
    Quiz.prototype.getQuestionIndex = function() {
    return this.questions[this.questionIndex];
    }
    //to check correct or wrong
    Quiz.prototype.guess = function(answer) {
    if (this.getQuestionIndex().isCorrectAnswer(answer)) {
    window.alert("Hurray!!..correct");
    this.questionIndex++;
    }else{
        window.alert("Wrong option,Please Try again");
        this.questionIndex
      }
    }
    
    Quiz.prototype.isEnded = function() {
    return this.questionIndex === this.questions.length;
    }
    
    // create quiz
    var quiz = new Quiz(questions);
    
    // display quiz
    quizResult();