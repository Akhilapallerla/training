let dailyCustomers = ["saketh","aparna","sai","mayuri","ravi"];
let productDetails = {
    "flour":50,
    "rice":150,
    "dal":10,
    "chocolate":25
};

function calculateCost(price,quantity){
        return price*quantity;
}

function calculateTotalDiscount(totalCost,discount,quantity,moreQuantity){
    
    if(quantity >= moreQuantity)
    {
        discount += 2;
    }
    let discountPrice = (totalCost*discount)/100;
    return discountPrice;
}

function displayMessage(product,quantity,productCost,purchasingCost){
    console.log(`product name : ${product}`);
    console.log(`quantity : ${quantity}`);
    console.log( `Total product Cost : ${productCost}`);
    console.log(`Total purchasing cost with discount : ${productCost-purchasingCost}`);
}

function calculateDiscount(customerName,productName,quantity){
    

    let dailyCustomerDiscount = 5;
    let newCustomerDiscount = 2;
    let moreQuantity = 5;
    let moreQuantityDiscount = 2;
    let flag = false;
    let totalDiscount = 0;

    for ( let value in dailyCustomers){
        
        if(dailyCustomers[value] == customerName){
            flag = true;
            totalDiscount += dailyCustomerDiscount;
            let price = productDetails[productName];
            let totalProductCost = calculateCost(price,quantity);
            let discountCost = calculateTotalDiscount(totalProductCost,totalDiscount,quantity,moreQuantity);
            displayMessage(productName,quantity,totalProductCost,discountCost);
        }
        
    }
    if(flag == false){
        totalDiscount += newCustomerDiscount;
            let price = productDetails[productName];
            let totalProductCost = calculateCost(price,quantity);
            let discountCost = calculateTotalDiscount(totalProductCost,totalDiscount,quantity,moreQuantity);
            displayMessage(productName,quantity,totalProductCost,discountCost);
    }
}

let customer = window.prompt("Enter customer name");
let product = window.prompt("Enter product Name");
let quantity = window.prompt("Enter quantity of product");
quantity = Number(quantity);
calcDiscount(customer,product,quantity);


