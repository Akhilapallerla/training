function charFreq(string) {
    var frequency = {};
    for (var i = 0; i < string.length; i++) {
      var character = string.charAt(i);
      if (frequency[character]) {
        frequency[character]++;
      } else {
        frequency[character] = 1;
      }
    }
    return frequency;
  };

var inputString= window.prompt("Enter the String: ");
var count = charFreq(inputString);
console.log(count);