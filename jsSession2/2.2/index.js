let numColumn= window.prompt("Enter column count:");
let figure = "";
if(numColumn%2==0){
    window.alert("Column count should be an odd integer only");
}else{
    for (let i = 1; i <= numColumn; i++) {
        // creating space
        for (let j = 1; j <= numColumn- i; j++) {
          figure += " ";
        }
        // creating numbers
        for (let k = 1; k <= 2 * i - 1; k++) {
          figure += k;
        }
        figure += "\n";
    }
}
console.log(figure);
