const data = {
    Maruti: [
      ["800", 1995],
      ["Alto", 2000],
      ["WagonR", 2002],
      ["Esteem", 2004],
      ["SX4", 2007],
    ],
    TATA: [
      ["Indica", 2001],
      ["Indigo", 2006],
      ["Safari", 2003],
      ["Sumo", 2001],
    ],
    Chevrolet:[
        ["Beat ",2006],
        ["Travera",2002],
        ["Spark",2007]
    ],
    Toyota:[
    ["Camry",2005],
    ["Etios",2010],
    ["Corolla",2003],
    ["Endeavour",2008]
    ]
  };
  let counter = 3;
  function setModelValue() {
    let BrandName = document.getElementById("sltBrandName").value;
  
    //Reset to default value
    document.getElementById(
      "sltModelName"
    ).innerHTML = `<option value="">Select Model Name</option>`;
    document.getElementById("sltYear").innerHTML = `<option value="">Select Year</option>`;
  
    //Populate data in drop down
    for (i in data) {
      if (i == BrandName) {
        data[i].map((value) => {
          document.getElementById("sltModelName").innerHTML += `<option value="${value[0]}">${value[0]}</option>`;
        });
      }
    }
  }
  
  function setModelYear() {
    let ModelName = document.getElementById("sltModelName").value;
    let BrandName = document.getElementById("sltBrandName").value;
    document.getElementById("sltYear").innerHTML = `<option value="">Select Year</option>`;
    let year = 0;
    for (i in data) {
      if (i == BrandName) {
        data[i].map((value) => {
          if (value[0] == ModelName) {
            year = value[1];
          }
        });
      }
    }
    const d = new Date();
    let currYear = d.getFullYear();
  
    //Populate the year data
    for (let i = year; i <= currYear; i = i + 1) {
      document.getElementById("sltYear").innerHTML += `<option value="${i}">${i}</option>`;
    }
  }
  function addProblem() {
    counter = 3;
    console.log(document.getElementById("problemField"));
    document.getElementById("problemField").innerHTML += ` <div class="form-details name"> 
    <input type="text" id="${"txtName" + counter}" /> </div>`;
    counter++;
  }
  
  function removeProblem() {
    let lastElement = document.getElementById("problemField").lastElementChild;
    let lastElementId = lastElement.lastElementChild.id;
    document.getElementById(lastElementId).remove();
    counter--;
  }
  