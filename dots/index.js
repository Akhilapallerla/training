let counter = 0;
let score = 0;
let duration = localStorage.getItem("duration");
let box = localStorage.getItem("box").split("-");

const color = [
  ["red", 20],
  ["blue", 18],
  ["green", 14],
  ["violet", 18],
  ["yellow", 16],
  ];
    
let min = 0;

// set gamming box size
document.getElementById("box").style.width = box[0] + "px";
document.getElementById("box").style.height = box[1] + "px";

// changing shape
let shape = localStorage.getItem("shape");
console.log(shape);
if (shape == "circle") {
  document.getElementById("dot").style.borderRadius = 55 + "%"; //for circle
} else {
  document.getElementById("dot").style.borderRadius = 0 + "%";  //for square
}


    const setIn = setInterval(() => {
      // to stop after specific time
      if (min >= duration) {
        clearInterval(setIn);
        window.alert("GAME OVER!!!" );
      }
      min++;

      //   for position
      const top = Math.random().toFixed(2) * 100;
      const right = Math.random().toFixed(2) * 100;
      document.getElementById("dot").style.top = `${top}%`;;
      document.getElementById("dot").style.right = `${right}%`;
      giveColor(counter);    //   to give color
    }, 2000 * box[2]); //dot time 

    function giveColor() {
      if (counter == color.length) {
        counter = 0;
      }
      //to change size of circle 
      document.getElementById("dot").style.width = color[counter][1] + "px";
      document.getElementById("dot").style.height = color[counter][1] + "px";
      document.getElementById("dot").style.backgroundColor = color[counter][0];
      counter++;
    }

    // adding event listener to update score
    document.getElementById("dot").addEventListener("click", updateScore);

    function updateScore() {
      const colorChoosed = document.getElementById("dot").style.backgroundColor;
      if (colorChoosed == "red") {
        score -= 2
      } else if (colorChoosed == "blue") {
        score += 1;
      }else if (colorChoosed == "green") {
        score += 3;
      }else if (colorChoosed == "violet") {
        score -= 1;
      }else if (colorChoosed == "yellow") {
        score += 2;
      }

      document.getElementById("score").innerText=score;
    }

    