function startgame(event) {
    event.preventDefault(); 
    let level = document.getElementsByName("level");
    let levelValue = 0;
       //choose level
    for (i = 0; i < level.length; i++) { 
      if (level[i].checked) {
        levelValue = level[i].value;
      }
    }
    let duration = document.getElementById("duration").value; //to choose time
    let shape = document.getElementsByName("shape"); 
    // Comment 1

    let shapeValue = 0;
    for (i = 0; i < shape.length; i++) { //square or circle
      if (shape[i].checked) {
        shapeValue = shape[i].value;
      }
    }
  
    if (levelValue == 0 || duration == "" || shapeValue == 0) { 
      alert("Please fill all the fields"); 
    } else {
      localStorage.setItem("box", levelValue); //store in the localstorage
      localStorage.setItem("duration", duration);
      localStorage.setItem("shape", shapeValue);
      location.replace("./index.html");
    }
  }